import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SecondText extends StatelessWidget {
  const SecondText({super.key, required this.name, required this.fontSize});
  final String name;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      style: TextStyle(
          height: 1.25,
          letterSpacing: 0.1,
          fontSize: fontSize.sp,
          fontWeight: FontWeight.w500,
          color: const Color.fromRGBO(255, 255, 255, 0.5)),
    );
  }
}
